<?php

/**
 * Created by IntelliJ IDEA.
 * User: qasim
 * Date: 2/11/15
 * Time: 1:16 AM
 */
class Exam extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("exam_model");
    }

    public function index()
    {

        $exam_data = $this->exam_model->get();

        echo json_encode($exam_data);
    }

    public function insert_exam()
    {

        if ($this->input->server("REQUEST_METHOD") === "POST") {

            $exam_json = file_get_contents("php://input");

            $exam_json = mb_convert_encoding($exam_json, "UTF-8");

            $exam = json_decode(trim($exam_json));

            Exam::json_error();

            $this->exam_model->insert($exam);

            echo "true";
        } else if ($this->input->server("REQUEST_METHOD") === "GET") {

        } else if ($this->input->server("REQUEST_METHOD") === "DELETE") {
        } else {

            echo "REQUEST_METHOD ERROR";

        }


        return;

    }

    public static function json_error()
    {
        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                echo ' - Maximum stack depth exceeded';
                break;
            case JSON_ERROR_CTRL_CHAR:
                echo ' - Unexpected control character found';
                break;
            case JSON_ERROR_SYNTAX:
                echo ' - Syntax error, malformed JSON';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                echo ' - Invalid or malformed JSON';
                break;
            case JSON_ERROR_UTF8:
                echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
        }
    }
}