<?php

/**
 * Created by IntelliJ IDEA.
 * User: qasim
 * Date: 2/11/15
 * Time: 11:10 PM
 */
class Visa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("visa_model");
    }

    public function index()
    {
        $data = $this->visa_model->get();
        echo json_encode($data);
    }

    public function get_country_data()
    {
        $data = $this->visa_model->get_country_data();
        echo json_encode($data);
    }
}