<?php

/**
 * Created by IntelliJ IDEA.
 * User: qasim
 * Date: 2/10/15
 * Time: 11:06 PM
 */
class Patient extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("patient_model");
    }

    public function index()
    {
        $result = $this->patient_model->get();
        echo json_encode($result);
    }

    public function get_all_for_exam_screen()
    {
        $patients = $this->patient_model->get_all_for_exam_screen();
        echo json_encode($patients);
    }

    public function upload()
    {

    }

    public function insert_patient()
    {

        if ($this->input->server("REQUEST_METHOD") === "POST") {

            $patient_json = file_get_contents("php://input");

            $patient_json = mb_convert_encoding($patient_json, "UTF-8");

            $patient = json_decode(trim($patient_json));

            Patient::json_error();

            $this->patient_model->insert($patient);

            echo "true";
        } else if ($this->input->server("REQUEST_METHOD") === "GET") {

        } else if ($this->input->server("REQUEST_METHOD") === "DELETE") {
        } else {

            echo "REQUEST_METHOD ERROR";

        }


        return;

    }

    public static function json_error()
    {
        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                echo ' - Maximum stack depth exceeded';
                break;
            case JSON_ERROR_CTRL_CHAR:
                echo ' - Unexpected control character found';
                break;
            case JSON_ERROR_SYNTAX:
                echo ' - Syntax error, malformed JSON';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                echo ' - Invalid or malformed JSON';
                break;
            case JSON_ERROR_UTF8:
                echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
        }
    }


}