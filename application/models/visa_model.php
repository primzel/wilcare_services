<?php

/**
 * Created by IntelliJ IDEA.
 * User: qasim
 * Date: 2/11/15
 * Time: 11:13 PM
 */
class visa_model extends CI_Model
{

    private $table = "visa";

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function get()
    {
        $data = $this->db->get($this->table);
        $result = $data->result_array();
        return $result;
    }

    function get_country_data()
    {
        $data = $this->db->select("country_id")->from("state")->group_by("country_id")->get()->result_array();
        $result = array();
        foreach ($data as $row) {
            $temp = array();
            $query = "SELECT us_state FROM `state` where country_id='" . $row["country_id"] . "' group by us_state ";
            $temp["country"] = $row["country_id"];
            $temp["states"] = $this->db->query($query)->result_array();
            $tempStates = array();
            foreach ($temp["states"] as $city) {
                $query = "select state_name from state where us_state='" . $city["us_state"] . "'";
                $city["city"] = $this->db->query($query)->result_array();
                $cities = array();
                foreach ($city["city"] as $c) {
                    array_push($cities, $c["state_name"]);
                }
                $city["city"] = $cities;
                array_push($tempStates, $city);
            }
            $temp["states"] = $tempStates;
            array_push($result, $temp);
        }

        return $result;
    }

}

?>