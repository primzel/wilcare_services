<?php

/**
 * Created by IntelliJ IDEA.
 * User: qasim
 * Date: 2/11/15
 * Time: 1:15 AM
 */
class exam_model extends CI_Model
{

    private $table = "exam_patient";

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function get()
    {
        $data = $this->db->get($this->table);
        $result = $data->result_array();
        return $result;
    }
}