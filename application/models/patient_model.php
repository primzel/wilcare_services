<?php

/**
 * Created by IntelliJ IDEA.
 * User: qasim
 * Date: 2/10/15
 * Time: 11:07 PM
 */
class patient_model extends CI_Model
{

    private $table = "patient_main";

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function get()
    {
        $data = $this->db->get($this->table);
        $result = $data->result_array();
        return $result;
    }

    public function get_patients_having_exam_today()
    {
        $query = "select * from patient_main inner join exam_patient on patient_main.patient_id=exam_patient.patient_id where exam_patient.exam_date=CURDATE()";
        $patients = $this->db->query($query)->result_array();
        return $patients;
    }

    public function get_all_for_exam_screen()
    {
        $sql = "SELECT patient_main.* FROM patient_main left outer join exam_patient on exam_patient.patient_id= patient_main.patient_id where exam_patient.patient_id is NULL";
        $patients = $this->db->query($sql)->result_array();
        return $patients;
    }


}

?>